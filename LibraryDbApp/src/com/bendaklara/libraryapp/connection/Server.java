package com.bendaklara.libraryapp.connection;

import com.bendaklara.libraryapp.controller.LibraryService;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

public class Server extends Thread {
    private Socket socket;
    private LibraryService libraryService;

    public Server(Socket socket) {
        this.socket = socket;
    }
    
    public void initLibraryService(LibraryService libraryService){
        this.libraryService = libraryService;
    }

    @Override
    public void run() {
        try {
            BufferedReader input = new BufferedReader(
                    new InputStreamReader(socket.getInputStream()));
            PrintWriter output = new PrintWriter(socket.getOutputStream(), true);

            while(true) {
                String request="a";
                String response = "";
                switch(request=input.readLine()){
                    case "all":
                        response=libraryService.listBooks().toServerString();
                        //System.out.println(response);
                        break;
                    case "available":
                        response=libraryService.listAvailableCopies().toServerString();
                        //System.out.println(response);
                        break;
                    case "loan":
                        response=libraryService.listBorrowedCopies().toServerString();
                        //System.out.println(response);
                        break;
                    default:
                        response="Ugh. Something went awry.";
                        break;
                }
                output.println(response);
            }

        } catch(IOException e) {
            System.out.println("Oops: " + e.getMessage());
        } finally {
            try {
                socket.close();
            } catch(IOException e) {
                // Oh, well!
            }
        }

    }
}
