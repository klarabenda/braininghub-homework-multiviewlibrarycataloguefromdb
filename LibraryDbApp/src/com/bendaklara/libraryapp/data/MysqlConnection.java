/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bendaklara.libraryapp.data;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Majka
 */
public class MysqlConnection {
    private Connection connection;
    String url, username, password;
    
    public MysqlConnection(String url, String username, String password) {

        this.url = url;
        this.username = username;
        this.password = password;
    }
    

    public Connection getConnection() {
        try {
            connection=DriverManager.getConnection(this.url, this.username, this.password);
        } catch (SQLException ex) {
            Logger.getLogger(MysqlConnection.class.getName()).log(Level.SEVERE, null, ex);
        }
        return connection;
    }    
    
    
}
