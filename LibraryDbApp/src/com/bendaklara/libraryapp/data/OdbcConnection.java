/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bendaklara.libraryapp.data;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Majka
 */
public class OdbcConnection {
    private static Connection con=null;
    private static ResultSet rs=null;
    private static String URL = "jdbc:oracle:thin:@localhost:1521:xe";
    private static String USERNAME = "username";
    private static String PASSWORD = "password";

    private OdbcConnection()  {
    }
    
    public static Connection getConnection() {
        try {
            Class.forName("oracle.jdbc.driver.OracleDriver");
            return DriverManager.getConnection(URL, USERNAME, PASSWORD);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(OdbcConnection.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
                Logger.getLogger(OdbcConnection.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
        
    }
    
    
}
