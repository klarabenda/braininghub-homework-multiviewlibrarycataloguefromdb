/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bendaklara.libraryapp.data;

import com.bendaklara.libraryapp.data.MysqlConnection;
import com.bendaklara.libraryapp.data.OdbcConnection;
import com.bendaklara.libraryapp.model.Author;
import com.bendaklara.libraryapp.model.AuthorCache;
import com.bendaklara.libraryapp.model.Book;
import com.bendaklara.libraryapp.model.BookCache;
import com.bendaklara.libraryapp.model.BookCopy;
import com.bendaklara.libraryapp.model.BookCopyCache;
import com.bendaklara.libraryapp.model.EBook;
import com.bendaklara.libraryapp.model.EBookCache;
import com.bendaklara.libraryapp.model.Reader;
import com.bendaklara.libraryapp.model.ReaderCache;
import com.bendaklara.libraryapp.model.Shelf;
import com.bendaklara.libraryapp.model.ShelfCache;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Majka
 */
public class QueryHandler {
    MysqlConnection mysqlConnection;
    private Statement statement;
    private static final String URL = "jdbc:mysql://localhost/library?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC";
    private static final String USERNAME = "username";
    private static final String PASSWORD = "password";    
    
    
    public boolean init(String type){
        boolean success=false; 
        switch(type){
            case "mysql":
                success=initMysqlConnection();
                break;
//            case "oracle":
//                success=initOracleConnection();
//                break;
        }
        return success;
    }
    
    public boolean initMysqlConnection(){
        mysqlConnection = new MysqlConnection(URL, USERNAME, PASSWORD);
        return true;
    }

//    public boolean initOracleConnection(){
//        try (Connection con=OdbcConnection.getConnection()) {
//            //statement = con.createStatement();
//            return true;
//        }   catch (SQLException ex) {
//            Logger.getLogger(QueryHandler.class.getName()).log(Level.SEVERE, null, ex);
//        }
//        return false;
//    }

    public Integer insertReader(String name){
        Integer readerId=0;
        try(Connection con = mysqlConnection.getConnection()) {
            statement = con.createStatement();
            String sql = "INSERT INTO Reader (name) VALUES ('"+name+"')" ;
            statement.executeUpdate(sql) ;
            sql = "SELECT MAX(readerId) as readerid FROM Reader";
            ResultSet rs = statement.executeQuery(sql) ;
            while(rs.next()){
                readerId=rs.getInt("readerid");
            }
        } catch (SQLException ex) {
            Logger.getLogger(QueryHandler.class.getName()).log(Level.SEVERE, null, ex);
        }
        return readerId;
    }

    public void insertSubscription(Reader reader, Book book){
        String isbn = book.getIsbn();
        int readerId=reader.getReaderId();
        try (Connection con = mysqlConnection.getConnection()) {
            statement = con.createStatement();
            String sql = "INSERT INTO Observer (isbn, readerId) VALUES ('"+isbn+"', "+ readerId+")" ;
            statement.executeUpdate(sql) ;
        } catch (SQLException ex) {
            Logger.getLogger(QueryHandler.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void deleteSubscription(Reader reader, Book book){
        String isbn = book.getIsbn();
        int readerId=reader.getReaderId();
        try (Connection con = mysqlConnection.getConnection()) {
            statement = con.createStatement();
            String sql = "DELETE FROM Observer "
                    + "WHERE isbn ='"+isbn+"' AND readerId = "+ readerId ;
            statement.executeUpdate(sql) ;
        } catch (SQLException ex) {
            Logger.getLogger(QueryHandler.class.getName()).log(Level.SEVERE, null, ex);
        }
    }    

    
    public void insertLoan(Integer readerId, Integer bookId, LocalDate startDate){
        try (Connection con = mysqlConnection.getConnection()) {
            statement = con.createStatement();
            String sql = "INSERT INTO BorrowBook (readerId, bookId, borrowDate) "
                    + "VALUES ("+ readerId+ " , "+ bookId + ", '" + startDate.toString() + "')" ;
            statement.executeUpdate(sql) ;
            sql = "UPDATE bookcopy SET ";
            
        } catch (SQLException ex) {
            Logger.getLogger(QueryHandler.class.getName()).log(Level.SEVERE, null, ex);
        }
    }


    public EBook insertEbook(Book book, String format, int size, int numberOfCopies){
        int ebookId=-1;
        try (Connection con = mysqlConnection.getConnection()) {
            statement = con.createStatement();
            String sql = "INSERT INTO Ebook (isbn, format, size, copies) "
                    + "VALUES ('"+ book.getIsbn()  + "', '"+ format + "', " + size+ ", " + numberOfCopies + ")" ;
            statement.executeUpdate(sql) ;
            sql = "SELECT MAX(ebookId) as ebookid FROM Ebook";
            ResultSet rs = statement.executeQuery(sql) ;
            while(rs.next()){
                ebookId=rs.getInt("ebookid");
            }
        } catch (SQLException ex) {
            Logger.getLogger(QueryHandler.class.getName()).log(Level.SEVERE, null, ex);
        }
        return new EBook(ebookId, book, size, format, numberOfCopies);
    }
    
    
    public Integer insertShelf(Author author, int capacity){
        Integer shelfId=-1;
        try (Connection con = mysqlConnection.getConnection()) {
            statement = con.createStatement();
            String sql = "INSERT INTO EbookShelf (authorId, currentCapacity, currentLoad) "
                    + "VALUES ("+ Integer.toString(author.getAuthorId()) + ", "+ Integer.toString(capacity) + ", 0)" ;
            statement.executeUpdate(sql) ;
            sql = "SELECT MAX(shelfId) as shelfid FROM EbookShelf";
            ResultSet rs = statement.executeQuery(sql) ;
            while(rs.next()){
                shelfId=rs.getInt("shelfid");
            }
        } catch (SQLException ex) {
            Logger.getLogger(QueryHandler.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return shelfId;
    }
    
    public void insertEbookToShelf(Shelf shelf, EBook ebook){
        try (Connection con = mysqlConnection.getConnection()) {
            int newLoad = shelf.getLoad()+ ebook.getSize();
            int copies = ebook.getNumberOfCopies();
            int shelfId = shelf.getShelfId();
            int ebookId = ebook.getEbookId();
            statement = con.createStatement();
            String sql = "INSERT INTO EbookToShelf (shelfId, ebookId) "
                    + "VALUES ("+ shelfId  + ", "+ ebookId + ")" ;
            statement.executeUpdate(sql) ;
            sql = "UPDATE EbookShelf SET currentLoad = " +newLoad+ " WHERE shelfId=" + shelfId;
            statement.executeUpdate(sql) ;

        } catch (SQLException ex) {
            Logger.getLogger(QueryHandler.class.getName()).log(Level.SEVERE, null, ex);
        }   
    }

    public void updateReader(Reader reader){
        Integer readerId=reader.getReaderId();
        String readerName=reader.getName();
        String readerAddress=reader.getAddress();
        try (Connection con = mysqlConnection.getConnection()) {
            statement = con.createStatement();
            String sql = "UPDATE Reader SET name='" +readerName+ "', address = '"+ readerAddress+ "' WHERE readerId =" + readerId ;
            statement.executeUpdate(sql) ;
        } catch (SQLException ex) {
            Logger.getLogger(QueryHandler.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void updateEBookCopies(EBook ebook){
        try (Connection con = mysqlConnection.getConnection()) {
            statement = con.createStatement();
            String sql = "UPDATE Ebook SET copies = "
                    + ebook.getNumberOfCopies() + " WHERE ebookId=" +ebook.getEbookId()  ;
            statement.executeUpdate(sql) ;
        } catch (SQLException ex) {
            Logger.getLogger(QueryHandler.class.getName()).log(Level.SEVERE, null, ex);
            }
        
    }

    public BookCache getBooks(){
        BookCache bookList= new BookCache();
        try (Connection con = mysqlConnection.getConnection()) {
            statement = con.createStatement();
            String sql = "SELECT a.isbn as isbn, b.title as title, a.authorId as authorid, c.name as authorname "
                    + "FROM book b "
                    + "LEFT JOIN bookauthor a "
                    + "ON a.isbn=b.isbn "
                    + "JOIN author c "
                    + "ON c.authorid = a.authorId ORDER BY isbn";
            ResultSet rs = statement.executeQuery(sql);
            while (rs.next()) {
                String isbn=rs.getString("isbn");
                String title=rs.getString("title");  
                Author author = new Author(rs.getString("authorname"), Integer.parseInt(rs.getString("authorid")));
                if(bookList.findByIsbn(isbn)){
                    bookList.getBookByIsbn(isbn).addAuthor(author);
                }
                else{
                    bookList.addItem(new Book(isbn,title, author));
                }
            }
        } catch (SQLException ex) {
            Logger.getLogger(QueryHandler.class.getName()).log(Level.SEVERE, null, ex);
        }
        return bookList;
    }

    public BookCopyCache getCopies(){
        BookCopyCache copyList= new BookCopyCache();
        try (Connection con = mysqlConnection.getConnection()) {
            statement = con.createStatement();
            String sql = "SELECT a.bookId as bookid, b.isbn as isbn, b.title as title "
                    + "FROM bookCopy a "
                    + "LEFT JOIN book b "
                    + "ON a.isbn=b.isbn "
                    + "ORDER BY bookid";
            ResultSet rs = statement.executeQuery(sql);
            while (rs.next()) {
                Integer copyId = rs.getInt("bookid");
                Book book = new Book(rs.getString("isbn"),rs.getString("title"));
                BookCopy copy = new BookCopy(copyId,book);
                if(!getCopyAvailability(copyId)){
                    copy.setAvailable(false);
                }
                copyList.addItem(copy);
            }
        } catch (SQLException ex) {
            Logger.getLogger(QueryHandler.class.getName()).log(Level.SEVERE, null, ex);
        }
        return copyList;
    }

    public ReaderCache getReaders(){
        ReaderCache readerCache = new ReaderCache();
        try (Connection con = mysqlConnection.getConnection()) {
            statement = con.createStatement();
            String sql = "SELECT readerId, name, address FROM Reader";
            ResultSet rs = statement.executeQuery(sql) ;
            while(rs.next()){
                Reader reader = new Reader(rs.getInt("readerid"), rs.getString("name"), rs.getString("address"));
                readerCache.addItem(reader);
            }
        } catch (SQLException ex) {
            Logger.getLogger(QueryHandler.class.getName()).log(Level.SEVERE, null, ex);
        }
        return readerCache;
    }
    
    public EBookCache getEBooks(){
        EBookCache ebookCache = new EBookCache();
        try (Connection con = mysqlConnection.getConnection()) {
            statement = con.createStatement();
            String sql = "SELECT a.ebookId as ebookid, a.isbn as isbn, a.format as format, "
                    + "a.size as size, a.copies as copies, b.title as title "
                    + "FROM Ebook a "
                    + "JOIN Book b "
                    + "ON a.isbn=b.isbn ";
            ResultSet rs = statement.executeQuery(sql) ;
            while(rs.next()){
                int ebookid=rs.getInt("ebookid"); 
                String isbn= rs.getString("isbn"); 
                String format = rs.getString("format"); 
                int size=rs.getInt("size"); 
                int copies=rs.getInt("copies"); 
                String title= rs.getString("title"); 
                ebookCache.addItem(new EBook(ebookid, isbn, title, size, format, copies) );
            }
        } catch (SQLException ex) {
            Logger.getLogger(QueryHandler.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return ebookCache;
    }
    
    public ReaderCache getReadersByBookSubscription(Book book){
        String isbn = book.getIsbn();
        ReaderCache readerCache= new ReaderCache();
        try (Connection con = mysqlConnection.getConnection()) {
            statement = con.createStatement();
            String sql = "SELECT o.readerId as readerid, r.name as name "
                    + "FROM Observer o "
                    + "JOIN Reader r "
                    + "ON o.readerId=r.readerId "
                    + "WHERE o.isbn= '"+isbn +"'";
            ResultSet rs = statement.executeQuery(sql);
            while (rs.next()) {
                int readerId=rs.getInt("readerid");
                String name=rs.getString("name");  
                readerCache.addItem(new Reader(readerId, name));
            }
        } catch (SQLException ex) {
            Logger.getLogger(QueryHandler.class.getName()).log(Level.SEVERE, null, ex);
        }
        return readerCache;
    }
    
    public AuthorCache getAuthorsByBook(Book book){
        String isbn = book.getIsbn();
        AuthorCache authorList= new AuthorCache();
        try (Connection con = mysqlConnection.getConnection()) {
            statement = con.createStatement();
            String sql = "SELECT b.authorId as authorid, a.name as name FROM bookauthor b "
                    + "LEFT JOIN author a "
                    + "ON a.authorid=b.authorid "
                    + "WHERE b.isbn= '"+isbn +"'";
            ResultSet rs = statement.executeQuery(sql);
            while (rs.next()) {
                String name=rs.getString("name");
                int authorid=rs.getInt("authorid");  
                authorList.addItem(new Author(name, authorid));
            }
        } catch (SQLException ex) {
            Logger.getLogger(QueryHandler.class.getName()).log(Level.SEVERE, null, ex);
        }
        return authorList;
    }
    
    public AuthorCache getAuthorsByName(String name){
        AuthorCache authors = new AuthorCache();
        try (Connection con = mysqlConnection.getConnection()) {
            statement = con.createStatement();
            String sql = "SELECT authorid, name "
                    + "FROM author "
                    + "WHERE name LIKE '%"+name+"%'";
            ResultSet rs = statement.executeQuery(sql);
            while (rs.next()) {
                Integer authorId = rs.getInt("authorid");
                String authorName = rs.getString("name");
                authors.addItem(new Author (authorName, authorId));
                }
        } catch (SQLException ex) {
            Logger.getLogger(QueryHandler.class.getName()).log(Level.SEVERE, null, ex);
        }
        return authors;
    } 

    public BookCache getBooksByAuthor(Author authorToFind){
        Integer authorId = authorToFind.getAuthorId();
        BookCache bookList= new BookCache();
        try (Connection con = mysqlConnection.getConnection()) {
            statement = con.createStatement();
            String sql = "SELECT a.isbn as isbn, b.title as title, a.authorId as authorid, c.name as authorname "
                    + "FROM book b "
                    + "LEFT JOIN bookauthor a "
                    + "ON a.isbn=b.isbn "
                    + "JOIN author c "
                    + "ON c.authorid = a.authorId "
                    + "WHERE a.authorid = "+authorId;
            ResultSet rs = statement.executeQuery(sql);
            while (rs.next()) {
                String isbn=rs.getString("isbn");
                String title=rs.getString("title");  
                Author author = new Author(rs.getString("authorname"), Integer.parseInt(rs.getString("authorid")));
                if(bookList.findByIsbn(isbn)){
                    bookList.getBookByIsbn(isbn).addAuthor(author);
                }
                else{
                    bookList.addItem(new Book(isbn,title, author));
                }
            }
        } catch (SQLException ex) {
            Logger.getLogger(QueryHandler.class.getName()).log(Level.SEVERE, null, ex);
        }
        return bookList;
    }

    public BookCache getBooksByAuthor(AuthorCache authorsToFind){
        List<Integer> authorIdList = authorsToFind.getItemIds();
        String expression= "(";
        for(Integer id: authorIdList){
            expression+=Integer.toString(id)+", ";
        }
        expression=expression.substring(0, expression.length()-2);
        expression+=")";

        BookCache bookList= new BookCache();
        try (Connection con = mysqlConnection.getConnection()) {
            statement = con.createStatement();
            String sql = "SELECT a.isbn as isbn, b.title as title, a.authorId as authorid, c.name as authorname "
                    + "FROM book b "
                    + "LEFT JOIN bookauthor a "
                    + "ON a.isbn=b.isbn "
                    + "JOIN author c "
                    + "ON c.authorid = a.authorId "
                    + "WHERE a.authorid IN "+expression;
            ResultSet rs = statement.executeQuery(sql);
            while (rs.next()) {
                String isbn=rs.getString("isbn");
                String title=rs.getString("title");  
                Author author = new Author(rs.getString("authorname"), Integer.parseInt(rs.getString("authorid")));
                if(bookList.findByIsbn(isbn)){
                    bookList.getBookByIsbn(isbn).addAuthor(author);
                }
                else{
                    bookList.addItem(new Book(isbn,title, author));
                }
            }
        } catch (SQLException ex) {
            Logger.getLogger(QueryHandler.class.getName()).log(Level.SEVERE, null, ex);
        }
        return bookList;
    }
    
    public BookCache getBooksByTitle(String titleToFind){
        BookCache bookList= new BookCache();
        try (Connection con = mysqlConnection.getConnection()) {
            statement = con.createStatement();
            String sql = "SELECT a.isbn as isbn, b.title as title, a.authorId as authorid, c.name as authorname "
                    + "FROM book b "
                    + "LEFT JOIN bookauthor a "
                    + "ON a.isbn=b.isbn "
                    + "JOIN author c "
                    + "ON c.authorid = a.authorId "
                    + "WHERE b.title LIKE '%"+titleToFind+"%'";
            ResultSet rs = statement.executeQuery(sql);
            while (rs.next()) {
                String isbn=rs.getString("isbn");
                String title=rs.getString("title");  
                Author author = new Author(rs.getString("authorname"), Integer.parseInt(rs.getString("authorid")));
                if(bookList.findByIsbn(isbn)){
                    bookList.getBookByIsbn(isbn).addAuthor(author);
                }
                else{
                    bookList.addItem(new Book(isbn,title, author));
                }
            }
        } catch (SQLException ex) {
            Logger.getLogger(QueryHandler.class.getName()).log(Level.SEVERE, null, ex);
        }
        return bookList;
    }

    public Optional<Book> getBookByIsbn(String isbn){
        Book book = null;
        try (Connection con = mysqlConnection.getConnection()) {
            statement = con.createStatement();
            String sql = "SELECT title FROM book "
                    + "WHERE isbn = '" + isbn+ "'";
            ResultSet rs = statement.executeQuery(sql);
            while (rs.next()) {
                String title=rs.getString("title");  
                book = new Book(isbn, title);
                return Optional.ofNullable(book);
            }
        } catch (SQLException ex) {
            Logger.getLogger(QueryHandler.class.getName()).log(Level.SEVERE, null, ex);
        }
        return Optional.ofNullable(book);
    }
    
    public BookCopyCache getCopiesByIsbn(String isbnToFind){
        BookCopyCache copyList= new BookCopyCache();
        try (Connection con = mysqlConnection.getConnection()) {
            statement = con.createStatement();
            String sql = "SELECT a.bookId as bookid, b.isbn as isbn, b.title as title "
                    + "FROM bookCopy a "
                    + "LEFT JOIN book b "
                    + "ON a.isbn=b.isbn "
                    + "WHERE a.isbn ="+isbnToFind;
            ResultSet rs = statement.executeQuery(sql);
            while (rs.next()) {
                Integer copyId = rs.getInt("bookid");
                Book book = new Book(rs.getString("isbn"),rs.getString("title"));
                BookCopy copy = new BookCopy(copyId,book);
                if(!getCopyAvailability(copyId)){
                    copy.setAvailable(false);
                }
                copyList.addItem(copy);
            }
        } catch (SQLException ex) {
            Logger.getLogger(QueryHandler.class.getName()).log(Level.SEVERE, null, ex);
        }
        return copyList;
    }

    public BookCopyCache getCopiesByIsbn(List<String> isbnToFind){
        BookCopyCache copyList= new BookCopyCache();
        try (Connection con = mysqlConnection.getConnection()) {
            statement = con.createStatement();
            String expression= "(";
            for(String isbn: isbnToFind){
                expression+=isbn +", ";
            }
            expression=expression.substring(0, expression.length()-2);
            expression+=")";
            String sql = "SELECT a.bookId as bookid, b.isbn as isbn, b.title as title "
                    + "FROM bookCopy a "
                    + "LEFT JOIN book b "
                    + "ON a.isbn=b.isbn "
                    + "WHERE a.isbn IN "+expression;
            ResultSet rs = statement.executeQuery(sql);
            while (rs.next()) {
                Integer copyId = rs.getInt("bookid");
                Book book = new Book(rs.getString("isbn"),rs.getString("title"));
                BookCopy copy = new BookCopy(copyId,book);
                if(!getCopyAvailability(copyId)){
                    copy.setAvailable(false);
                }
                copyList.addItem(copy);
            }
        } catch (SQLException ex) {
            Logger.getLogger(QueryHandler.class.getName()).log(Level.SEVERE, null, ex);
        }
        return copyList;
    }
    
    public Reader getReaderById(Integer readerId){
        String readerName="";
        String readerAddress="";        
        try (Connection con = mysqlConnection.getConnection()) {
            statement = con.createStatement();
            String sql = "SELECT name, address FROM Reader WHERE readerId="+Integer.toString(readerId);
            ResultSet rs = statement.executeQuery(sql) ;
            while(rs.next()){
                readerName=rs.getString("name");
                readerAddress=rs.getString("address");
            }
        } catch (SQLException ex) {
            Logger.getLogger(QueryHandler.class.getName()).log(Level.SEVERE, null, ex);
        }
        return new Reader(readerId, readerName, readerAddress);
    }
    
    
    public ReaderCache getReadersByName(String name){
        ReaderCache readerCache = new ReaderCache();
        try (Connection con = mysqlConnection.getConnection()) {
            statement = con.createStatement();
            String sql = "SELECT readerId, name, address FROM Reader WHERE name LIKE '%"+name+"%'";
            ResultSet rs = statement.executeQuery(sql) ;
            while(rs.next()){
                Reader reader = new Reader(rs.getInt("readerid"), rs.getString("name"), rs.getString("address"));
                readerCache.addItem(reader);
            }
        } catch (SQLException ex) {
            Logger.getLogger(QueryHandler.class.getName()).log(Level.SEVERE, null, ex);
        }
        return readerCache;
    }

    public ShelfCache getShelvesByAuthor(Author author){
        ShelfCache shelfCache = new ShelfCache();
        try (Connection con = mysqlConnection.getConnection()) {
            statement = con.createStatement();
            String sql = "SELECT shelfid, currentCapacity, currentLoad FROM EbookShelf WHERE authorId ="+Integer.toString(author.getAuthorId());
            ResultSet rs = statement.executeQuery(sql) ;
            while(rs.next()){
                Shelf shelf = new Shelf(rs.getInt("shelfid"), rs.getInt("currentcapacity"), rs.getInt("currentload"), author);
                shelfCache.addItem(shelf);
            }
        } catch (SQLException ex) {
            Logger.getLogger(QueryHandler.class.getName()).log(Level.SEVERE, null, ex);
        }
        return shelfCache;
    }


    public EBookCache getEBooksByBook(Book book){
        EBookCache ebookList= new EBookCache();
        String title=book.getTitle();
        String isbn = book.getIsbn();
        try (Connection con = mysqlConnection.getConnection()) {
            statement = con.createStatement();
            String sql = "SELECT ebookId,isbn,format,size,copies FROM ebook "
                    + "WHERE isbn = '"+isbn+"'";
            ResultSet rs = statement.executeQuery(sql);
            while (rs.next()) {
                int ebookId = rs.getInt("ebookid");
                String format=rs.getString("format");
                int size=rs.getInt("size");
                int copies=rs.getInt("copies");
                EBook ebook = new EBook(ebookId, isbn, title, size, format, copies);
                ebookList.addItem(ebook);
            }
        } catch (SQLException ex) {
            Logger.getLogger(QueryHandler.class.getName()).log(Level.SEVERE, null, ex);
        }
        return ebookList;
    }

    public Optional<EBook> getEBookByBookAndFormat(Book book, String format){
        String title=book.getTitle();
        String isbn = book.getIsbn();
        EBook ebook=null;
        try (Connection con = mysqlConnection.getConnection()) {
            statement = con.createStatement();
            String sql = "SELECT ebookId,isbn,format,size,copies FROM ebook "
                    + "WHERE isbn = '"+isbn+"' AND format = '" +format + "'";
            ResultSet rs = statement.executeQuery(sql);
            while (rs.next()) {
                int ebookId = rs.getInt("ebookid");
                int size=rs.getInt("size");
                int copies=rs.getInt("copies");
                ebook=new EBook(ebookId, isbn, title, size, format, copies);
                return Optional.ofNullable(ebook);
            }
        } catch (SQLException ex) {
            Logger.getLogger(QueryHandler.class.getName()).log(Level.SEVERE, null, ex);
        }
        return Optional.ofNullable(ebook);
    }
    
    public boolean getCopyAvailability(int copyId){
        boolean available=true;
        try (Connection con = mysqlConnection.getConnection()) {
            statement = con.createStatement();
            String sql = "SELECT bookid from borrowbook where returndate is null and bookid =" + copyId;
            ResultSet rs = statement.executeQuery(sql);
            while(rs.next()){
                available=false;
            }
        } catch (SQLException ex) {
            Logger.getLogger(QueryHandler.class.getName()).log(Level.SEVERE, null, ex);
        }
        return available;
    }

    
    public boolean readerIdExists(Integer readerId){
        boolean success = false;
        try (Connection con = mysqlConnection.getConnection()) {
            statement = con.createStatement();
            String sql = "SELECT readerId FROM Reader WHERE readerId ="+readerId;
            ResultSet rs = statement.executeQuery(sql) ;
            while(rs.next()){
                success=true;
            }
        } catch (SQLException ex) {
            Logger.getLogger(QueryHandler.class.getName()).log(Level.SEVERE, null, ex);
        }
        return success;
    }
    
    public boolean bookIdExists(Integer bookId){
        boolean success = false;
        try (Connection con = mysqlConnection.getConnection()) {
            statement = con.createStatement();
            String sql = "SELECT bookId FROM BookCopy WHERE bookId ="+bookId;
            ResultSet rs = statement.executeQuery(sql) ;
            while(rs.next()){
                success=true;
            }
        } catch (SQLException ex) {
            Logger.getLogger(QueryHandler.class.getName()).log(Level.SEVERE, null, ex);
        }
        return success;
    }
    
    public boolean subscriptionExists(Reader reader, Book book){
        int readerId=reader.getReaderId();
        String isbn =  book.getIsbn();
        boolean success = false;
        try (Connection con = mysqlConnection.getConnection()) {
            statement = con.createStatement();
            String sql = "SELECT isbn FROM Observer "
                    + "WHERE readerId ="+readerId 
                    + " AND isbn ='" + isbn+ "'";
            ResultSet rs = statement.executeQuery(sql) ;
            while(rs.next()){
                success=true;
            }
        } catch (SQLException ex) {
            Logger.getLogger(QueryHandler.class.getName()).log(Level.SEVERE, null, ex);
        }
        return success;
    }
    
    
}
