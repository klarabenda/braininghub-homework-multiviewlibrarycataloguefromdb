/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bendaklara.libraryapp.view;

import com.bendaklara.libraryapp.controller.LibraryService;
import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.awt.GridLayout;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import static javax.swing.JFrame.EXIT_ON_CLOSE;
import javax.swing.JPanel;
import javax.swing.JTextField;

/**
 *
 * @author Majka
 */
public class SwingView extends JFrame implements IView {

    private JPanel panelContainer;
    private CardLayout cardLayout;
    private MenuView panelMenu;
    private RegistrationView panelRegister;
    private ListView panelList;
    private LibraryService libraryService;
    private BooksByAuthorView panelBooksByAuthor;
    private BooksByTitleView panelBooksByTitle;
    private LoanView panelLoan;
    private EbookView panelEbook;

    public SwingView() {
        init();
    }

    public void init() {
        setDefaultCloseOperation(EXIT_ON_CLOSE);

        setLocation(0, 0);
        setTitle("Library App");
        setSize(1200, 600);
        panelContainer = new JPanel();
        cardLayout = new CardLayout();
        panelMenu = new MenuView();
        panelRegister = new RegistrationView();
        panelList = new ListView();
        panelBooksByAuthor = new BooksByAuthorView();
        panelBooksByTitle = new BooksByTitleView();
        panelLoan = new LoanView();
        panelEbook = new EbookView();
        
        panelContainer.setLayout(cardLayout);

        panelContainer.add(panelMenu, "1");
        panelContainer.add(panelList, "2");
        panelContainer.add(panelBooksByAuthor, "3");
        panelContainer.add(panelBooksByTitle, "4");
        panelContainer.add(panelRegister, "5");
        panelContainer.add(panelLoan, "6");
        panelContainer.add(panelEbook, "7");
        add(panelContainer);
        
        cardLayout.show(panelContainer, "1");
        pack();
        setVisible(true);
    }

    public void setController(LibraryService libraryService) {
        this.libraryService = libraryService;
    }

    @Override
    public void showMenu() {
        if (!cardLayout.equals(null)) {
            cardLayout.show(panelContainer, "1");
        }
    }

    @Override
    public void doMenuListBooks() {
        panelList.setController(libraryService);
        cardLayout.show(panelContainer, "2");
    }

    @Override
    public void doMenuListAvailableBooks() {
        panelList.setController(libraryService);
        cardLayout.show(panelContainer, "2");
    }

    @Override
    public void doMenuListBorrowedBooks() {
        panelList.setController(libraryService);
        cardLayout.show(panelContainer, "2");
    }

    @Override
    public void doMenuListBooksByAuthor() {
        panelBooksByAuthor.setController(libraryService);
        cardLayout.show(panelContainer, "3");
    }

    @Override
    public void doMenuListBooksByTitle() {
        panelBooksByTitle.setController(libraryService);
        cardLayout.show(panelContainer, "4");
    }

    @Override
    public void doMenuRegisterNewReader() {
        panelRegister.setController(libraryService);
        cardLayout.show(panelContainer, "5");
    }

    @Override
    public void doMenuLoan() {
        panelLoan.setController(libraryService);
        panelLoan.populateLists();
        cardLayout.show(panelContainer, "6");
    }

    @Override
    public void doMenuAddEbook() {
        panelEbook.setController(libraryService);
        panelEbook.populateLists();
        cardLayout.show(panelContainer, "7");
    }

    private class MenuView extends JPanel implements ActionListener {

        private GridLayout grid;
        private JTextField title;
        private JButton doMenuListBooks, doMenuListAvailableBooks, doMenuListBorrowedBooks, doMenuListBooksByAuthor;
        private JButton doMenuListBooksByTitle, doMenuRegisterNewReader, doMenuAddEbook, doMenuLoan;

        public MenuView() {
            init();
        }

        private void init() {
            grid = new GridLayout(9, 1);
            this.setLayout(grid);
            setPreferredSize(new Dimension(1200, 600));
            title = new JTextField("MENU");
            doMenuListBooks = new JButton("List books");
            doMenuListAvailableBooks = new JButton("List available books");
            doMenuListBorrowedBooks = new JButton("List borrowed books");
            doMenuListBooksByAuthor = new JButton("Find books by an author");
            doMenuListBooksByTitle = new JButton("Find books by title");
            doMenuRegisterNewReader = new JButton("Register new reader");
            doMenuLoan = new JButton("Make reader loan");
            doMenuAddEbook = new JButton("Add ebook");
            
            doMenuListBooks.setActionCommand("1");
            doMenuListAvailableBooks.setActionCommand("2");
            doMenuListBorrowedBooks.setActionCommand("3");
            doMenuListBooksByAuthor.setActionCommand("4");
            doMenuListBooksByTitle.setActionCommand("5");
            doMenuRegisterNewReader.setActionCommand("6");
            doMenuLoan.setActionCommand("7");
            doMenuAddEbook.setActionCommand("8");
            
            doMenuListBooks.addActionListener(this);
            doMenuListAvailableBooks.addActionListener(this);
            doMenuListBorrowedBooks.addActionListener(this);
            doMenuListBooksByAuthor.addActionListener(this);
            doMenuListBooksByTitle.addActionListener(this);
            doMenuRegisterNewReader.addActionListener(this);
            doMenuLoan.addActionListener(this);
            doMenuAddEbook.addActionListener(this);

            this.add(title);
            this.add(doMenuListBooks);
            this.add(doMenuListAvailableBooks);
            this.add(doMenuListBorrowedBooks);
            this.add(doMenuListBooksByAuthor);
            this.add(doMenuListBooksByTitle);
            this.add(doMenuRegisterNewReader);
            this.add(doMenuLoan);
            this.add(doMenuAddEbook);
        }

        @Override
        public void actionPerformed(ActionEvent event) {
            String action = event.getActionCommand();
            libraryService.doSelected(Integer.parseInt(action));
        }

    }

}
