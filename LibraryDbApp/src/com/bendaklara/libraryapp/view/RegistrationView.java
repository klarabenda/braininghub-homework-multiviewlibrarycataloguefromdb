/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bendaklara.libraryapp.view;

import com.bendaklara.libraryapp.controller.LibraryService;
import com.bendaklara.libraryapp.model.Reader;
import java.awt.Dimension;
import java.awt.GridLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

/**
 *
 * @author Majka
 */
class RegistrationView extends JPanel {

    private GridLayout grid;
    private JTextField name, address, title, newReaderDataFeedback;
    private JLabel nameLabel, addressLabel;
    private JButton submitButton, showMenuButton;
    private LibraryService libraryService;
    private Reader newReader;

    public RegistrationView() {
        init();
    }

    public void setController(LibraryService libraryService) {
        this.libraryService = libraryService;

    }

    private void init() {
        grid = new GridLayout(8, 1);
        this.setLayout(grid);
        setPreferredSize(new Dimension(1200, 600));
        title = new JTextField("New User Registration");
        name = new JTextField();
        address = new JTextField();
        nameLabel = new JLabel("Full name");
        addressLabel = new JLabel("Address");
        newReaderDataFeedback = new JTextField("");
        submitButton = new JButton("Save User");
        submitButton.addActionListener((e) -> {
            if (this.libraryService==null) {
                System.out.println("Action listener. Library service is null");
            }
            if (validateFields()) {
                newReader = libraryService.addNewReader(name.getText());
                libraryService.updateReaderAddress(newReader, address.getText());
                newReader.setAddress(address.getText());
                newReaderDataFeedback.setText(newReader.toString());
                showMenuButton.setVisible(true);
            } else {
                newReaderDataFeedback.setText("User is not added. One or more fields are empty.");
            }
        });

        showMenuButton = new JButton("Show Menu");
        showMenuButton.addActionListener((e) -> {
            libraryService.doSelected(9);
        });
        showMenuButton.setVisible(false);
        this.add(title);
        this.add(nameLabel);
        this.add(name);
        this.add(addressLabel);
        this.add(address);
        this.add(submitButton);
        this.add(newReaderDataFeedback);
        this.add(showMenuButton);
        
    }

    public boolean validateFields() {
        boolean success = true;
        if (name.getText().equals("")) {
            success = false;
        }
        if (address.getText().equals("")) {
            success = false;
        }
        return success;
    }

}
