/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bendaklara.libraryapp.view.client;

import com.bendaklara.libraryapp.connection.Server;
import com.bendaklara.libraryapp.controller.LibraryService;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PipedInputStream;
import java.io.PrintWriter;
import java.io.StringBufferInputStream;
import java.io.StringReader;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.charset.StandardCharsets;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

/**
 *
 * @author Majka
 */
public class ClientListView extends JFrame {

    private static InputStream query;
    private GridLayout grid, gridButtons;
    private JTextField title;
    private JTextArea books;
    private JScrollPane booksScrollPane;
    private JButton allBooksButton, filterAvailableButton, filterNotAvailableButton;
    private JPanel buttonPanel;
    String booklist = "";
    private final int PORT = 5000;

    public ClientListView() {
        init();
    }

    private void init() {
        this.setDefaultCloseOperation(EXIT_ON_CLOSE);

        this.setLocation(0, 0);
        this.setTitle("Library App");
        this.setSize(1200, 600);

        grid = new GridLayout(1, 2);
        gridButtons = new GridLayout(3, 1);
        this.setLayout(grid);
        this.setPreferredSize(new Dimension(1200, 600));
//        title = new JTextField("List books");
        books = new JTextArea();
        books.setLineWrap(true);
        books.setWrapStyleWord(true);
        books.setEditable(false);
        books.setFont(new Font("Serif", Font.PLAIN, 20));
        booksScrollPane = new JScrollPane(books);
        booksScrollPane.setPreferredSize(new Dimension(50, 200));
        allBooksButton = new JButton("Show titles owned by the library");
        allBooksButton.setActionCommand("all");
        allBooksButton.addActionListener(new ClientButtonListener());
        filterAvailableButton = new JButton("Show copies in library");
        filterAvailableButton.setActionCommand("available");
        filterAvailableButton.addActionListener(new ClientButtonListener());
        filterNotAvailableButton = new JButton("Show copies on loan");
        filterNotAvailableButton.addActionListener(new ClientButtonListener());
        filterNotAvailableButton.setActionCommand("loan");
        buttonPanel = new JPanel();
        buttonPanel.setLayout(gridButtons);
        //this.add(title);
        this.add(buttonPanel);
        buttonPanel.add(allBooksButton);
        buttonPanel.add(filterAvailableButton);
        buttonPanel.add(filterNotAvailableButton);
        this.add(booksScrollPane);
        this.setVisible(true);
    }

    class ClientButtonListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            try (Socket socket = new Socket("localhost", PORT)) {
                BufferedReader serverResponse = new BufferedReader(
                        new InputStreamReader(socket.getInputStream()));
                PrintWriter queryToServer = new PrintWriter(socket.getOutputStream(), true);
                String response;
                System.out.println(e.getActionCommand());
                queryToServer.println(e.getActionCommand());
                response = serverResponse.readLine().replaceAll("---", "\n");
                books.setText(response);

            } catch (IOException ex) {
                Logger.getLogger(ClientButtonListener.class.getName()).log(Level.SEVERE, null, ex);
                System.out.println("Client Error: " + ex.getMessage());
            }

        }

    }

}
