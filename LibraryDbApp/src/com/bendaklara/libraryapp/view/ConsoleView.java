/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bendaklara.libraryapp.view;

import com.bendaklara.libraryapp.controller.LibraryService;
import com.bendaklara.libraryapp.model.Book;
import com.bendaklara.libraryapp.model.EBook;
import com.bendaklara.libraryapp.model.Reader;
import java.util.Optional;

/**
 *
 * @author Majka
 */
public class ConsoleView implements IView {
    LibraryService libraryService;


    @Override
    public void setController(LibraryService libraryService) {
        this.libraryService=libraryService;
    }

    @Override
    public void showMenu() {
        int choice = 0;
            System.out.println("------------");
            System.out.println("1. list all books");
            System.out.println("2. list available books");
            System.out.println("3. list books on loan");
            System.out.println("4. find book by author");
            System.out.println("5. find book by title");
            System.out.println("6. register new member");
            System.out.println("7. do loan");
            System.out.println("8. add ebook");
            System.out.println("9. menu");
            System.out.println("0. QUIT");
            System.out.println("------------");
            choice = getChoice();
            if(choice>0){
                libraryService.doSelected(choice);
                showMenu();
            }
            else{
                System.out.println("Goodbye.");
            }
    }

    private int getChoice() {
        int choice = 0;
        boolean success = true;
        do {
            choice = extra.Console.readInt("Type the number of your choice:     ");
            if (choice > 9 || choice < 0) {
                success = false;
                System.out.println(choice + " is not available in the menu.");
            }
        } while (!success);
        return choice;
    }

    @Override
    public void doMenuListBooks() {
        System.out.println("Our books");
        System.out.println(libraryService.listBooks());
        
    }

    @Override
    public void doMenuListAvailableBooks() {
        System.out.println("Available books");
        System.out.println(libraryService.listAvailableCopies());
    }

    @Override
    public void doMenuListBorrowedBooks() {
        System.out.println("Books on loan");
        System.out.println(libraryService.listBorrowedCopies());
    }

    @Override
    public void doMenuListBooksByAuthor() {
        String author = extra.Console.readLine("Author's name (type in as much as you can remember):   ");
        System.out.println(libraryService.listBooksByAuthor(author)   );
    }

    @Override
    public void doMenuListBooksByTitle() {
        String title = extra.Console.readLine("Title (type in as much as you can remember):   ");
        System.out.println(libraryService.listBooksByTitle(title)   );
    }

    @Override
    public void doMenuRegisterNewReader() {
        String readerName = extra.Console.readLine("Reader's full name:   ");
        Reader newReader = libraryService.addNewReader(readerName);
        String readerAddress = extra.Console.readLine("Reader's address:   ");
        newReader=libraryService.updateReaderAddress(newReader, readerAddress);
        System.out.println(newReader);
    }

    @Override
    public void doMenuLoan() {
        findReaderByName();
        
    }

    public void findReaderByName() {
        String readerName = extra.Console.readLine("Reader's full name:   ");
        System.out.println("Here are the readers with that name:");
        System.out.println(libraryService.listReadersByName(readerName));
    }

    public void doLoan() {
        Integer readerId=0;
        Integer bookId =0;
        boolean success=false;
        do{
            readerId = extra.Console.readInt("The reader id for the loan:   ");
            success=libraryService.readerIdValid(readerId);
            if(!success){
                System.out.println("This user id is not valid, please type the id again");
            }
        } while(!success);
        
        doMenuListAvailableBooks();
        success=false;
        do{
            bookId = extra.Console.readInt("The book id for the loan:   ");
            success=libraryService.bookIdValid(bookId);
            if(!success){
                System.out.println("This book id is not valid, please type an id again");
            }
        } while(!success);
        libraryService.registerLoan(readerId, bookId);
    }

    @Override
    public void doMenuAddEbook() {
        String format="";
        String isbn;
        Optional<Book> book;
        do{
            isbn= extra.Console.readLine("ISBN:   ");
            book = libraryService.findBookByIsbn(isbn);
            if(!book.isPresent()){
                System.out.println("No book in the library for this ISBN.");
            }
        }while(!book.isPresent());
        do{
            format= extra.Console.readLine("Format (up to 4 characters) :   ");
        }while(format.length()>4);
        int size = extra.Console.readInt("Size of ebook:   ");
        int copies = extra.Console.readInt("Number of copies to add:   ");
        Optional<EBook> ebook = libraryService.addEBook(book.get(), size, format, copies);
        if(ebook.isPresent()){
            System.out.println("Ebook on file : " + ebook.get());
        }
        else{
            System.out.println("A system error occurred. No ebook added.");
        }
        
    }
    
}
