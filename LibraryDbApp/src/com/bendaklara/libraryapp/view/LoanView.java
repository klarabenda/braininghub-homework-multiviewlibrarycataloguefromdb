/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bendaklara.libraryapp.view;

import com.bendaklara.libraryapp.controller.LibraryService;
import com.bendaklara.libraryapp.model.Book;
import com.bendaklara.libraryapp.model.BookCopy;
import com.bendaklara.libraryapp.model.Reader;
import com.bendaklara.libraryapp.model.ReaderCache;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.time.LocalDate;
import java.util.List;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

/**
 *
 * @author Majka
 */
class LoanView extends JPanel {
    private GridLayout gridSearch;
    private BorderLayout border;
    private JPanel buttonPanel, listPanel, subscribePanel;
    private JList readerList, bookList;
    private JButton makeLoanButton, subscribeButton, unsubscribeButton, showMenuButton;
    private JTextField loanFeedback;
    private LibraryService libraryService;
    private DefaultListModel readermodel, bookmodel;
    int readerSelection, bookSelection;
    List<Reader> readers;
    List<BookCopy> copies;

    public LoanView() {
        init();
    }

    public void setController(LibraryService libraryService) {
        this.libraryService = libraryService;
    }

    private void init() {
        border = new BorderLayout() ;
        gridSearch = new GridLayout(1,2);
        setPreferredSize(new Dimension(1200, 600));
        this.setLayout(border);
        readerList=new JList();
        bookList=new JList();
        listPanel=new JPanel();
        listPanel.setLayout(gridSearch);
        listPanel.add(readerList);
        listPanel.add(bookList);
        this.add(listPanel,BorderLayout.CENTER);
        showMenuButton= new JButton("Show menu");
        showMenuButton.addActionListener((e) -> {
            libraryService.doSelected(9);
        });
        subscribeButton = new JButton("Subscribe user for ebook");
        subscribeButton.addActionListener((e) -> {
            readerSelection = readerList.getSelectedIndex();
            bookSelection = bookList.getSelectedIndex();
            if(validateSelection()){
                Reader reader = readers.get(readerSelection);
                Book book = copies.get(bookSelection).getBook();
                String feedbackMessage="Subscription exists.";
                if(!libraryService.subscriptionExists(reader,book)){
                    libraryService.registerSubscription(reader, book);
                    feedbackMessage="Subscription added.";
                }
                loanFeedback.setText(feedbackMessage);
            }
        });
        unsubscribeButton = new JButton("Unsubscribe user from ebook");
        unsubscribeButton.addActionListener((e) -> {
            readerSelection = readerList.getSelectedIndex();
            bookSelection = bookList.getSelectedIndex();
            if(validateSelection()){
                Reader reader = readers.get(readerSelection);
                Book book = copies.get(bookSelection).getBook();
                String feedbackMessage="Subscription does not exist.";
                if(libraryService.subscriptionExists(reader,book)){
                   libraryService.removeSubscription(reader, book);
                   feedbackMessage="Subscription removed.";
                }
                loanFeedback.setText(feedbackMessage);
            }            
        });
        makeLoanButton = new JButton("Make loan");
        makeLoanButton.addActionListener((e) -> {
            readerSelection = readerList.getSelectedIndex();
            bookSelection = bookList.getSelectedIndex();
            if(validateSelection()){
                //System.out.println(readerSelection);
                //System.out.println(bookSelection);
                libraryService.registerLoan(readers.get(readerSelection).getReaderId(), copies.get(bookSelection).getBookId());
                loanFeedback.setText("Book " + copies.get(bookSelection) + 
                        " on loan to " + readers.get(readerSelection).getName() +
                        " until " + LocalDate.now().plusDays(15).toString() ) ;
                populateLists();
            }
            else{
                loanFeedback.setText("You have to select the reader and the book");
            }
            
        });
        loanFeedback=new JTextField("Choose a reader and a book, and register the loan.");
        buttonPanel = new JPanel();
        buttonPanel.setLayout(new GridLayout(4, 1));
        subscribePanel=new JPanel();
        subscribePanel.setLayout(new GridLayout(1, 2));
        subscribePanel.add(subscribeButton);
        subscribePanel.add(unsubscribeButton);
        buttonPanel.add(subscribePanel);
        buttonPanel.add(makeLoanButton);
        buttonPanel.add(showMenuButton);
        buttonPanel.add(loanFeedback);
        this.add(buttonPanel,BorderLayout.SOUTH);
    }

    public void populateLists(){
        readermodel = new DefaultListModel<String>();
        readers = (libraryService.listReaders()).toArrayList();
        for(Reader r : readers){
            readermodel.addElement(r.toString());
        }    
        readerList.setModel(readermodel);     
        
        bookmodel = new DefaultListModel<String>();
        copies = (libraryService.listAvailableCopies()).toArrayList();
        for(BookCopy c : copies){
            bookmodel.addElement(c.toString());
        }    
        bookList.setModel(bookmodel);     
        readerSelection=-1;
        bookSelection=-1;
    }
    
    public boolean validateSelection(){
        boolean success=true;
        if(readerSelection<0 || bookSelection < 0){
            success=false;
        }
        return success;
    }
}
