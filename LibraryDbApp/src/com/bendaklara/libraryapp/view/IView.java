/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bendaklara.libraryapp.view;

import com.bendaklara.libraryapp.controller.LibraryService;


/**
 *
 * @author Theme
 */
public interface IView {
    
    public void setController(LibraryService con);
    
    public void showMenu();
    
    public void doMenuListBooks();
    public void doMenuListAvailableBooks();
    public void doMenuListBorrowedBooks();
    
    public void doMenuListBooksByAuthor();
    public void doMenuListBooksByTitle();
    public void doMenuRegisterNewReader();
    public void doMenuAddEbook();
    public void doMenuLoan();

    //public void findReaderByName();
    //public void doLoan();

}
