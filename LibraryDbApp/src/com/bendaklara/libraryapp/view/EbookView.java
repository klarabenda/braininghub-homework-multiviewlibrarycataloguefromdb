/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bendaklara.libraryapp.view;

import com.bendaklara.libraryapp.controller.LibraryService;
import com.bendaklara.libraryapp.model.Book;
import com.bendaklara.libraryapp.model.BookCopy;
import com.bendaklara.libraryapp.model.EBook;
import com.bendaklara.libraryapp.model.Reader;
import com.bendaklara.libraryapp.model.ReaderCache;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;
import java.util.List;
import java.util.Optional;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

/**
 *
 * @author Majka
 */
class EbookView extends JPanel {
    private LibraryService libraryService;

    private BorderLayout border;
    private JPanel buttonPanel, listPanel, formatPanel;
    private JList bookList;
    private JLabel formatLabel;
    private JTextArea ebookList, ebookFeedback;
    private JButton addEbookButton, showMenuButton;
    private JTextField format;
    private JScrollPane ebooksScrollPane, booksScrollPane, feedbackScrollPane;

    private DefaultListModel ebookmodel, bookmodel;
    int bookSelection;
    String ebooks, formatInput;
    List<Book> books;

    public EbookView() {
        init();
    }

    public void setController(LibraryService libraryService) {
        this.libraryService = libraryService;
    }

    private void init() {
        border = new BorderLayout() ;
        setPreferredSize(new Dimension(1200, 600));
        this.setLayout(border);
        format = new JTextField();
        formatLabel = new JLabel("Format of ebook");
        ebookFeedback=new JTextArea("Select a book to add as ebook.");
        ebookFeedback.setEditable(false);
        ebookFeedback.setFont(new Font("Serif", Font.PLAIN, 16));
        ebookFeedback.setLineWrap(true);
        ebookFeedback.setWrapStyleWord(true);
        feedbackScrollPane = new JScrollPane(ebookFeedback); 
        feedbackScrollPane.setPreferredSize(new Dimension(50, 60));
        ebookList=new JTextArea();
        ebookList.setFont(new Font("Serif", Font.PLAIN, 16));
        ebookList.setLineWrap(true);
        ebookList.setWrapStyleWord(true);
        ebookList.setEditable(false);
        bookList=new JList();
        bookList.setFont(new Font("Serif", Font.PLAIN, 16));
        listPanel=new JPanel();
        listPanel.setLayout(new GridLayout(1,2));
        ebooksScrollPane = new JScrollPane(ebookList); 
        ebooksScrollPane.setPreferredSize(new Dimension(50, 200));
        booksScrollPane = new JScrollPane(bookList); 
        booksScrollPane.setPreferredSize(new Dimension(50, 200));
        listPanel.add(booksScrollPane);
        listPanel.add(ebooksScrollPane);
        this.add(listPanel,BorderLayout.CENTER);
        showMenuButton= new JButton("Show menu");
        showMenuButton.addActionListener((e) -> {
            libraryService.doSelected(9);
        });
        addEbookButton = new JButton("Add as ebook");
        addEbookButton.addActionListener((e) -> {
            bookSelection = bookList.getSelectedIndex();
            formatInput = format.getText();
            if(validateSelection()){
                Optional<EBook> ebookUpdate= libraryService.addEBook(books.get(bookSelection), 10, "pdf", 1);
                String feedback = "System error. No ebook added.";
                if(ebookUpdate.isPresent()){
                    ReaderCache readers = libraryService.listReadersByBookSubscription(books.get(bookSelection));
                    feedback="Result of update: " + ebookUpdate.get().toString();
                    if(readers.isEmpty()){
                        feedback+="\nNo subscribers for the ebook.";
                    }
                    else{
                        feedback+="\n Email update on availability sent for these readers: ";
                        feedback+=readers.toString();
                    }
                }
                ebookFeedback.setText(feedback) ;
                format.setText("");
                populateLists();
            }
        });
        formatPanel = new JPanel();
        formatPanel.setLayout(new GridLayout(1, 2));
        formatPanel.add(formatLabel);
        formatPanel.add(format);
        buttonPanel = new JPanel();
        buttonPanel.setLayout(new GridLayout(4,1));
        buttonPanel.add(formatPanel);
        buttonPanel.add(addEbookButton);
        buttonPanel.add(showMenuButton);
        buttonPanel.add(feedbackScrollPane);
        this.add(buttonPanel,BorderLayout.SOUTH);
    }

    public void populateLists(){
        ebooks = libraryService.listEBooks().toString();
        ebookList.setText(ebooks);
        bookmodel = new DefaultListModel<String>();
        books = (libraryService.listBooks()).toArrayList();
        for(Book b : books){
            bookmodel.addElement(b.toString());
        }    
        bookList.setModel(bookmodel);     
        bookSelection=-1;
    }
    
    public boolean validateSelection(){
        boolean success=true;
        if(bookSelection < 0 ){
            success=false;
            ebookFeedback.setText("No book selected. You need to select a book to add as ebook.");
        }
        if(formatInput.length()>4){
            success=false;
            ebookFeedback.setText("The format is four characters max.");
        }

        return success;
    }
}
