/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bendaklara.libraryapp.view;

import com.bendaklara.libraryapp.controller.LibraryService;
import com.bendaklara.libraryapp.model.Reader;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

/**
 *
 * @author Majka
 */
class BooksByTitleView extends JPanel {
    private GridLayout grid, gridButtons;
    private JLabel searchLabel;
    private JTextField searchText, emptySearchFeedback; 
    private JTextArea books;
    private JScrollPane booksScrollPane;    
    private JButton searchButton, showMenuButton;
    private LibraryService libraryService;
    private JPanel buttonPanel;
    String booklist="";

    public BooksByTitleView() {
        init();
    }

    public void setController(LibraryService libraryService) {
        this.libraryService = libraryService;
    }

    private void init() {
        grid = new GridLayout(1, 2);
        gridButtons = new GridLayout(5,1);
        this.setLayout(grid);
        
        setPreferredSize(new Dimension(1200, 600));
//        title = new JTextField("List books");
        books=new JTextArea();
        books.setLineWrap(true);
        books.setWrapStyleWord(true);
        books.setEditable(false);
        books.setFont(new Font("Serif", Font.PLAIN, 20));
        booksScrollPane = new JScrollPane(books); 
        booksScrollPane.setPreferredSize(new Dimension(50, 200));
        books.setText("Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.\n"
                + "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum");
        searchLabel = new JLabel("Text to search for in title");
        searchText = new JTextField();
        searchButton = new JButton("Search");
        searchButton.addActionListener((e) -> {
            if(validateFields()){
                booklist = (libraryService.listBooksByTitle(searchText.getText())).toString();
                books.setText(booklist);
                emptySearchFeedback.setText("");
            }
            else{
                emptySearchFeedback.setText("Search text field is empty.");
            }
        });
        showMenuButton= new JButton("Show menu");
        showMenuButton.addActionListener((e) -> {
            libraryService.doSelected(9);
        });
        emptySearchFeedback = new JTextField();
        emptySearchFeedback.setEditable(false);

        buttonPanel = new JPanel();
        buttonPanel.setLayout(gridButtons);
        //this.add(title);
        buttonPanel.add(searchLabel);
        buttonPanel.add(searchText);
        buttonPanel.add(searchButton);
        buttonPanel.add(showMenuButton);
        buttonPanel.add(emptySearchFeedback);

        this.add(buttonPanel);
        
        this.add(booksScrollPane);
    }

    public boolean validateFields() {
        System.out.println("Validating searchfield");
        boolean success = true;
        if (searchText.getText().equals("")) {
            success = false;
        }
        return success;
    }
    
}
