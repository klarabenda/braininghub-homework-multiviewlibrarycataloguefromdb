/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bendaklara.libraryapp.controller;

import com.bendaklara.libraryapp.connection.Server;
import com.bendaklara.libraryapp.data.QueryHandler;
import com.bendaklara.libraryapp.model.Author;
import com.bendaklara.libraryapp.model.AuthorCache;
import com.bendaklara.libraryapp.model.Book;
import com.bendaklara.libraryapp.model.BookCache;
import com.bendaklara.libraryapp.model.BookCopyCache;
import com.bendaklara.libraryapp.model.EBook;
import com.bendaklara.libraryapp.model.EBookCache;
import com.bendaklara.libraryapp.model.Reader;
import com.bendaklara.libraryapp.model.ReaderCache;
import com.bendaklara.libraryapp.model.Shelf;
import com.bendaklara.libraryapp.model.ShelfCache;
import com.bendaklara.libraryapp.view.ConsoleView;
import com.bendaklara.libraryapp.view.IView;
import com.bendaklara.libraryapp.view.SwingView;
import java.io.IOException;
import java.net.ServerSocket;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 *
 * @author Majka
 */
public class LibraryService {
    private QueryHandler queryHandler;
    private IView view;

    public LibraryService(QueryHandler queryHandler) {
        this.queryHandler=queryHandler;
    }
    
    public void startServer(int port){
	try(ServerSocket serverSocket = new ServerSocket(port)) {
        while(true) {
                  Server libraryServer = new Server(serverSocket.accept());
                  libraryServer.initLibraryService(this);
                  libraryServer.start();
//                Socket socket = serverSocket.accept();
//                Server echoer = new Server(socket);
//                echoer.start();
            }
        } catch(IOException e) {
            System.out.println("Server exception " + e.getMessage());
        }                
    }

    public void doResponseToClient(int selected) {
        switch(selected) {
            case 1: view.doMenuListBooks(); break;
        }
    }        

    public void initView(int viewNumber){
        if (viewNumber == 0)
            view = new ConsoleView();
        else 
            view = new SwingView();

        view.setController(this);
        view.showMenu();
    }
    
    public BookCopyCache listCopies(){
        return queryHandler.getCopies();
    }

    public BookCache listBooks(){
        return queryHandler.getBooks();
    }
    
    public BookCopyCache listBookCopies(){
        return queryHandler.getCopies();
    }    

    
    public ReaderCache listReaders(){
        return queryHandler.getReaders();
    }
    
    public EBookCache listEBooks(){
        return queryHandler.getEBooks();
    }
    
    public BookCopyCache listAvailableCopies(){
        BookCopyCache availableCopies = queryHandler.getCopies().filterCopiesOnAvailability(true);
        return availableCopies;
    }    

    public BookCopyCache listBorrowedCopies(){
        BookCopyCache borrowedCopies = queryHandler.getCopies().filterCopiesOnAvailability(false);
        return borrowedCopies;
    }    

    public BookCache listBooksByAuthor(String author){
        return queryHandler.getBooksByAuthor(queryHandler.getAuthorsByName(author));
    }    
    
    public BookCache listBooksByTitle(String title){
        return queryHandler.getBooksByTitle(title);
    }    
    
    public BookCopyCache listCopiesForBookCache(BookCache bookCache){
        return queryHandler.getCopiesByIsbn(bookCache.getIsbns());
    }
        
    public ReaderCache listReadersByName(String name){
        return queryHandler.getReadersByName(name);
    }

    public ReaderCache listReadersByBookSubscription(Book book){
        return queryHandler.getReadersByBookSubscription(book);
    }
    
    public Reader addNewReader(String name){
        Integer readerId = queryHandler.insertReader(name);
        return new Reader(readerId, name);
    }

    public Reader updateReaderAddress(Reader reader, String address){
        reader.setAddress(address);
        queryHandler.updateReader(reader);
        return queryHandler.getReaderById(reader.getReaderId());
    }
    
    
    public boolean readerIdValid(Integer readerId){
        return queryHandler.readerIdExists(readerId);
    }

    public boolean bookIdValid(Integer bookId){
        return queryHandler.bookIdExists(bookId);
    }
    
    public boolean subscriptionExists(Reader reader, Book book){
        return queryHandler.subscriptionExists(reader, book);
    }

    public void registerSubscription(Reader reader, Book book) {
        queryHandler.insertSubscription(reader, book);
    }

    public void removeSubscription(Reader reader, Book book) {
        queryHandler.deleteSubscription(reader, book);
    }

    
    public void registerLoan(Integer readerId, Integer bookId) {
        LocalDate startDate = LocalDate.now();
        queryHandler.insertLoan(readerId, bookId, startDate);
    }

    public Optional<EBook> addEBook(Book book, int size, String format, int copies){
        Optional<EBook> foundBook= queryHandler.getEBookByBookAndFormat(book, format);
        if(foundBook.isPresent()){
            EBook ebook = foundBook.get();
            ebook.addCopies(copies);
            queryHandler.updateEBookCopies(ebook);
        }
        else {
            EBook ebook=queryHandler.insertEbook(book, format, size, copies);
            ShelfCache shelvesToAddEbook = findShelvesToAddEBook(ebook);
            for(Shelf shelf : shelvesToAddEbook.toArrayList()){
                queryHandler.insertEbookToShelf(shelf, ebook);
            }
        }
        return queryHandler.getEBookByBookAndFormat(book, format);
    }
    
    public Shelf addNewShelfForAuthor (Author author){
        Integer shelfId = queryHandler.insertShelf(author, 100);
        return new Shelf(shelfId, 100, author);
    }
        
    public ShelfCache findShelvesToAddEBook(EBook ebook){
        ShelfCache foundShelves=new ShelfCache();
        AuthorCache authors = queryHandler.getAuthorsByBook(ebook.getBook());
        for(Author author: authors.toArrayList()){
            ShelfCache authorShelfCache=queryHandler.getShelvesByAuthor(author);
            if(authorShelfCache.isEmpty()){
                foundShelves.addItem(addNewShelfForAuthor(author));
            }
            else{
                Optional<Shelf> foundShelf=findShelfWithSpace(ebook.getSize(), authorShelfCache);
                if(foundShelf.isPresent()){
                    foundShelves.addItem(addNewShelfForAuthor(author));
                }
                else{
                    foundShelves.addItem(addNewShelfForAuthor(author));
                }
            }
        }
        return foundShelves;
    }
    
    public Optional<Shelf> findShelfWithSpace(int spaceNeeded, ShelfCache shelfCache){
        Shelf foundShelf = null;
        for(Shelf shelf : shelfCache.toArrayList()){
            if(shelf.getCapacity() - shelf.getLoad() >= spaceNeeded){
                return Optional.ofNullable(shelf) ;
            }
        }
        return Optional.ofNullable(foundShelf) ;
    }

    public Optional<Book> findBookByIsbn(String isbn){
        return queryHandler.getBookByIsbn(isbn);
    }


    public void doSelected(int selected) {
        switch(selected) {
            case 1: view.doMenuListBooks(); break;
            case 2: view.doMenuListAvailableBooks(); break;
            case 3: view.doMenuListBorrowedBooks();break;
            case 4: view.doMenuListBooksByAuthor();break;
            case 5: view.doMenuListBooksByTitle();break;
            case 6: view.doMenuRegisterNewReader();break;
            case 7: view.doMenuLoan();break;
            case 8: view.doMenuAddEbook();break;
            case 9: view.showMenu();break;
        }
    }        

    
}
