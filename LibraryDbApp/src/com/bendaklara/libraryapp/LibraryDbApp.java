/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bendaklara.libraryapp;

import com.bendaklara.libraryapp.connection.Server;
import com.bendaklara.libraryapp.data.QueryHandler;
import com.bendaklara.libraryapp.controller.LibraryService;
import com.bendaklara.libraryapp.model.Book;
import java.io.IOException;
import java.net.ServerSocket;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 *
 * @author Majka
 */
public class LibraryDbApp {
    private static QueryHandler queryHandler;
    private static LibraryService libraryService;
    private static final int PORT=5000;

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        //startApp(AppMode.SERVER);
    	startApp(AppMode.SWING);

    }
    
    private static void startApp(AppMode mode){
        queryHandler=new QueryHandler();
        System.out.println("DB initialized: " + queryHandler.init("mysql"));
        libraryService = new LibraryService(queryHandler);
        switch(mode){
            case SERVER:
                libraryService.startServer(PORT);
                break;
            case CONSOLE:
                libraryService.initView(0);
                break;
            case SWING:
                libraryService.initView(1);
                break;
        }
    }
    private enum AppMode{ SERVER, CONSOLE, SWING }
    
}

//import static something model.SelectCollection.MAX_KOLCSONZES;
//ezutan MAX