/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bendaklara.libraryapp.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Majka
 */
public class BookCache extends Cache<Book> implements Serializable  {
    
    public boolean findByIsbn(String isbn) {
        for(Book book: cache){
            if(book.getIsbn().equals(isbn)){
                return true;
            }
        }
        return false;
    }
    
    public Book getBookByIsbn (String isbn) {
        for(Book book: cache){
            if(book.getIsbn().equals(isbn)){
                return book;
            }
        }
        return null;
    }
    
    public List<String> getIsbns (){
        List<String> isbnList = new ArrayList<>();
        for(Book book: cache){
            isbnList.add(book.getIsbn());
        }
        return isbnList;
    }

    @Override
    public String toString() {
        String message="";
        for(Book book: cache){
            message+=book.toString() +"\n\n" ;
        }
        return message;
    }
    
    
}
