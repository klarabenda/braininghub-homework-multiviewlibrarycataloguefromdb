/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bendaklara.libraryapp.model;

import com.bendaklara.libraryapp.model.interfaces.Cachable;
import java.util.List;

/**
 *
 * @author Majka
 */
public class BookCopy implements Cachable {
    private int bookId;
    private Book book;
    private boolean available;

    public BookCopy(int bookId, Book book) {
        this.bookId = bookId;
        this.book=book;
        this.available=true;
    }

    public int getBookId() {
        return bookId;
    }

    

    public void setBookId(int bookId) {
        this.bookId = bookId;
    }

    public Book getBook() {
        return book;
    }

    public void setBook(Book book) {
        this.book = book;
    }
    
    public String getIsbn() {
        return this.book.getIsbn();
    }

    public String getTitle() {
        return this.book.getTitle();
    }

    public List<Author> getAuthors() {
        return this.book.getAuthors();
    }

    public boolean isAvailable() {
        return available;
    }

    public void setAvailable(boolean available) {
        this.available = available;
    }

    @Override
    public String toString() {
        return "Id: " + bookId + ", " + book ;
    }

    @Override
    public int getId() {
        return getBookId();
    }
    
}
