/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bendaklara.libraryapp.model;

import com.bendaklara.libraryapp.model.interfaces.Cachable;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Majka
 */
public class Shelf implements Cachable {
    private int shelfId, capacity, load;
    private Author author;
    private EBookCache ebooks;

    public Shelf(int shelfId, int capacity, Author author) {
        this.shelfId = shelfId;
        this.capacity = capacity;
        this.load=0;
        this.author = author;
        this.ebooks = new EBookCache();
    }
    
    public Shelf(int shelfId, int capacity, int load, Author author) {
        this(shelfId, capacity, author);
        this.load=load;
    }

    public void addEBook(EBook ebook){
        ebooks.addItem(ebook);
    }

    public Integer getShelfId() {
        return shelfId;
    }

    public Integer getAuthorId() {
        return author.getAuthorId();
    }

    public Integer getCapacity() {
        return capacity;
    }

    public void setCapacity(Integer capacity) {
        this.capacity = capacity;
    }

    public Integer getLoad() {
        return load;
    }

    public void setLoad(Integer load) {
        this.load = load;
    }

    public Author getAuthor() {
        return author;
    }

    @Override
    public int getId() {
        return getShelfId();
    }    
}
