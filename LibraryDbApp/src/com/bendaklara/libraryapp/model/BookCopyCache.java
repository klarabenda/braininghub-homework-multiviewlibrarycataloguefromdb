/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bendaklara.libraryapp.model;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 *
 * @author Majka
 */
public class BookCopyCache extends Cache<BookCopy> {

    public boolean findByIsbn(String isbn) {
        for(BookCopy copy: cache){
            Book book = copy.getBook();
            if(book.getIsbn().equals(isbn)){
                return true;
            }
        }
        return false;
    }

    public BookCache getBooksByIsbn(String isbn) {
        BookCache foundbooks = new BookCache();
        for(BookCopy copy: cache){
            Book book = copy.getBook();
            if(book.getIsbn().equals(isbn)){
                foundbooks.addItem(book);
            }
        }
        return foundbooks;
    }
   
    public BookCopyCache getCopiesByIsbn(String isbn) {
        BookCopyCache foundbooks = new BookCopyCache();
        for(BookCopy copy: cache){
            Book book = copy.getBook();
            if(book.getIsbn().equals(isbn)){
                foundbooks.addItem(copy);
            }
        }
        return foundbooks;
    }

    public BookCopyCache filterCopiesOnAvailability(boolean available) {
        BookCopyCache foundbooks = new BookCopyCache();
        for(BookCopy copy: cache){
            if(copy.isAvailable()==available){
                foundbooks.addItem(copy);
            }
        }
        return foundbooks;
    }
}
