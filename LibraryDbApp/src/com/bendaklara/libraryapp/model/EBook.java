/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bendaklara.libraryapp.model;

import com.bendaklara.libraryapp.model.interfaces.Cachable;
import com.bendaklara.libraryapp.model.interfaces.IObservable;
import com.bendaklara.libraryapp.model.interfaces.IObserver;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Majka
 */
public class EBook implements Cachable {
    int ebookId;
    private Book book;
    private int size, numberOfCopies;
    private String format;
    private boolean inStock;

    public EBook(int ebookId, Book book, int size, String format) {
        this.ebookId=ebookId;
        this.book = book;
        this.size = size;
        this.format=format;
        numberOfCopies=0;
    }

    public EBook(int ebookId, Book book, int size, String format, int numberOfCopies) {
        this(ebookId, book, size, format);
        this.numberOfCopies = numberOfCopies;
    }
    
    public EBook(int ebookId, String isbn, String title, int size, String format, int numberOfCopies){
        this(ebookId, new Book(isbn, title), size, format, numberOfCopies);
    }

    public List<Author> getAuthors() {
        return this.getBook().getAuthors();
    }
        
    public Book getBook() {
        return book;
    }

    public int getSize() {
        return size;
    }

    public String getFormat() {
        return format;
    }

    public int getNumberOfCopies() {
        return numberOfCopies;
    }

    public void setNumberOfCopies(int numberOfCopies) {
        this.numberOfCopies = numberOfCopies;
    }
    
    public void addCopies(int numberOfCopiesToAdd) {
        this.numberOfCopies= this.numberOfCopies+numberOfCopiesToAdd;
    }

    public int getEbookId() {
        return ebookId;
    }

    @Override
    public int getId() {
        return getEbookId();
    }
    
    @Override
    public String toString() {
        return book + "\nebookId: " +ebookId+ ", format: " + format+ ", size: " + size + ", copies: " + numberOfCopies;
    }

    
    
}
