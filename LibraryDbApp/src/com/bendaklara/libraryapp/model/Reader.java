/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bendaklara.libraryapp.model;

import com.bendaklara.libraryapp.model.interfaces.Cachable;
import com.bendaklara.libraryapp.model.interfaces.IObserver;

/**
 *
 * @author Majka
 */
public class Reader implements Cachable, IObserver  {
    private int readerId;
    private String name;
    private String address;

    public Reader(int readerId, String name) {
        this.readerId = readerId;
        this.name = name;
    }

    public Reader(int readerId, String name, String address) {
        this(readerId, name);
        this.address=address;
    }

    public int getReaderId() {
        return readerId;
    }

    public void setReaderId(int readerId) {
        this.readerId = readerId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    @Override
    public String toString() {
        return "ReaderId: " + readerId + ", Name: " + name + ", Address: " + address;
    }

    @Override
    public int getId() {
        return getReaderId();
    }

    @Override
    public void update() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void unSubscribe() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
