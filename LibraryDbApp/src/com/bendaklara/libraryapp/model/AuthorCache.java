/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bendaklara.libraryapp.model;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Majka
 */
public class AuthorCache extends Cache<Author> {

    @Override
    public String toString() {
        String message="";
        for(Object o: cache){
            Author author = (Author)o;
            message+=author.toString() +", " ;
        }
        if(message.length()>2){
            message=message.substring(0, message.length()-2);            
        }
        return message;
    }
}
