/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bendaklara.libraryapp.model.interfaces;

/**
 *
 * @author Majka
 */
public interface IObserver {
    
     public void update();
     public void unSubscribe();
    
}
