/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bendaklara.libraryapp.model;

import com.bendaklara.libraryapp.model.interfaces.Cachable;
import com.bendaklara.libraryapp.model.interfaces.IObservable;
import com.bendaklara.libraryapp.model.interfaces.IObserver;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Majka
 */
public class Book implements Cachable, IObservable, Serializable {
    private String isbn, title;
    private List<Author> authors;
    private List<IObserver> readers = new ArrayList<>();
    private ReaderCache subscribedReaders;


    public Book(String isbn, String title) {
        this.isbn = isbn;
        this.title = title;
        this.authors = new ArrayList<>();
    }
    
    public Book(String isbn, String title, Author author) {
        this(isbn, title);
        authors.add(author);
    }    


    @Override
    public void add(IObserver observer) {
        readers.add(observer);
    }

    @Override
    public void remove(IObserver observer) {
        
    }

    @Override
    public void notifyObservers() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    public String getIsbn() {
        return isbn;
    }

    public void setIsbn(String isbn) {
        this.isbn = isbn;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public List<Author> getAuthors() {
        return authors;
    }

    public void setAuthors(List<Author> authors) {
        this.authors = authors;
    }

    public void addAuthor(Author author) {
        this.authors.add(author);
    }

    @Override
    public String toString() {
        String message = "Isbn: " + isbn + ", " + title ;
        if(authors.toArray().length>0){
            message = message +" by " + authors.toString().substring(1, authors.toString().length()-1) ; 
        }
        return message;
    }

    @Override
    public int getId() {
        return Integer.parseInt(getIsbn());
    }
    
}
