/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bendaklara.libraryapp.model;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Majka
 */
public class EBookCache extends Cache<EBook>  {
    
    public boolean findByIsbn(String isbn) {
        for(EBook ebook: cache){
            Book book = ebook.getBook();
            if(book.getIsbn().equals(isbn)){
                return true;
            }
        }
        return false;
    }
    
    public BookCache getBooksByIsbn (String isbn) {
        BookCache books = new BookCache();
        for(EBook ebook: cache){
            Book book = ebook.getBook();
            if(book.getIsbn().equals(isbn)){
                books.addItem(book);
            }
        }
        return books;
    }
    
    public List<String> getIsbns (){
        List<String> isbnList = new ArrayList<>();
        for(EBook ebook: cache){
            Book book = ebook.getBook();
            String isbn = book.getIsbn();
            if(!isbnList.contains(isbn)){
                isbnList.add(isbn);
            }
        }
        return isbnList;
    }

}
