/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bendaklara.libraryapp.model;

import com.bendaklara.libraryapp.model.interfaces.Cachable;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Majka
 */

public class Cache<T extends Cachable> {
    protected List<T> cache;

    public Cache() {
        this.cache = new ArrayList<>();
    }

    public List<T> getCache() {
        return cache;
    }
    
    public List<T> toArrayList(){
        List<T> list = new ArrayList<>();
        for(T t: cache){
            list.add(t);
        }
        return list;
    }
    
    public String[] toStringArray (){
        List<String> list = new ArrayList<>();
        for(T t: cache){
            list.add(t.toString());
        }
        return (String[]) (list.toArray());
    }

    public void addItem(T t){
        cache.add(t);
    }

    public List<Integer> getItemIds (){
        List<Integer> idList = new ArrayList<>();
        for(T t: cache){
            idList.add(t.getId());
        }
        return idList;
    }
    
    public boolean isEmpty(){
        return cache.isEmpty();
    }
    
    public String toString() {
        String message="";
        for(T t: cache){
            message+=t.toString() +"\n" ;
        }
        return message;
    }
    
    public String toServerString() {
        String message="";
        for(T t: cache){
            message+=t.toString() +"---" ;
        }
        return message;
    }
    
}
