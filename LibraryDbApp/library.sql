create database library character set UTF8 collate utf8_bin;

use library;

create table Book (
isbn varchar(13),
title varchar(256),
primary key (isbn)
);

create table BookCopy (
bookId int(32) AUTO_INCREMENT,
isbn varchar(13),
primary key (bookId),
foreign key (isbn) references
Book(isbn)
);

create table Author (
authorId int(32) AUTO_INCREMENT,
name varchar(64),
primary key (authorId)
);


create table BookAuthor (
isbn varchar(13),
authorId int(32),
foreign key (isbn) references
Book(isbn),
foreign key (authorId) references
Author(authorId),
CONSTRAINT BookAuthorKey PRIMARY KEY (isbn,authorId)
);

create table Reader (
readerId int(32) AUTO_INCREMENT,
name varchar(64),
address varchar(256),
primary key (readerId)
);

create table BorrowBook (
readerId int(32),
bookId int(32),
borrowDate date,
returnDate date,
foreign key (readerId) references
Reader(readerId),
foreign key (bookId) references
BookCopy(BookId),
CONSTRAINT BorrowKey PRIMARY KEY (readerId, bookId, borrowDate)
);


create table EbookShelf (
shelfId int(32) AUTO_INCREMENT,
authorId int(32), 
currentCapacity int(10),
currentLoad int(10),
primary key (shelfId),
foreign key (authorId) references
Author(authorId)
);

create table Ebook (
ebookId int(32) AUTO_INCREMENT,
isbn varchar(13),
format varchar(5),
size int(10),
copies int(10),
primary key (ebookId),
foreign key (isbn) references
Book(isbn)
);

create table EbookToShelf (
shelfId int(32),
ebookId int(32),
foreign key (shelfId) references
Ebookshelf(shelfId),
foreign key (ebookId) references
Ebook(ebookId),
CONSTRAINT EbookToShelfKey PRIMARY KEY (shelfId,ebookId)
);


create table BorrowEbook (
ebookId int(32),
readerId int(32),
borrowDate date,
returnDate date,
foreign key (ebookId) references
Ebook(ebookId),
foreign key (readerId) references
Reader(readerId),
CONSTRAINT BookAuthorKey PRIMARY KEY (ebookId,readerId)
);

create table Observer (
isbn varchar(13),
readerId int(32),
foreign key (isbn) references
Book(isbn),
foreign key (readerId) references
Reader(readerId),
CONSTRAINT ObserverPrimaryKey PRIMARY KEY (isbn,readerId)
);

INSERT INTO BorrowBook (readerId, bookId, borrowDate, returnDate) VALUES (1, 21, '2018-10-01','2018-10-15');
INSERT INTO BorrowBook (readerId, bookId, borrowDate, returnDate) VALUES (1, 22, '2018-10-01','2018-10-15');
INSERT INTO BorrowBook (readerId, bookId, borrowDate, returnDate) VALUES (1, 24, '2018-10-01','2018-10-21');
INSERT INTO BorrowBook (readerId, bookId, borrowDate) VALUES (1, 28, '2019-01-10');
INSERT INTO BorrowBook (readerId, bookId, borrowDate) VALUES (1, 32, '2019-01-10');
INSERT INTO BorrowBook (readerId, bookId, borrowDate) VALUES (1, 30, '2019-01-10');


INSERT INTO Book (title, isbn) VALUES ('Design Patterns: Elements of Reusable Object-Oriented Software', '9780201633610');
INSERT INTO Book (title, isbn) VALUES ('Head First Design Patterns: A Brain-Friendly Guide', '9780596007126');
INSERT INTO Book (title, isbn) VALUES ('Head First Java', '9780596009205');
INSERT INTO Book (title, isbn) VALUES ('Data and Reality: A Timeless Perspective on Perceiving and Managing Information in Our Imprecise World', '9781935504214');
INSERT INTO Book (title, isbn) VALUES ('MySQL Cookbook: Solutions for Database Developers and Administrators', '9781449374020');
INSERT INTO Book (title, isbn) VALUES ('Java Generics and Collections: Speed Up the Java Development Process', '9780596527754');
INSERT INTO Book (title, isbn) VALUES ('Effective Java', '9780134685991');
INSERT INTO Book (title, isbn) VALUES ('Java 8 Lambdas: Functional Programming For The Masses', '9781449370770');
INSERT INTO Book (title, isbn) VALUES ('Version Control with Git: Powerful tools and techniques for collaborative software development', '9781449316389');
INSERT INTO Book (title, isbn) VALUES ('Git Pocket Guide: A Working Introduction', '9781449325862');
INSERT INTO Book (title, isbn) VALUES ('Intro to Java Programming, Comprehensive Version', '9780133761313');


INSERT INTO BookCopy (isbn) VALUES ('9780201633610');
INSERT INTO BookCopy (isbn) VALUES ('9780596007126');
INSERT INTO BookCopy (isbn) VALUES ('9780596009205');
INSERT INTO BookCopy (isbn) VALUES ('9781935504214');
INSERT INTO BookCopy (isbn) VALUES ('9781449374020');
INSERT INTO BookCopy (isbn) VALUES ('9780596527754');
INSERT INTO BookCopy (isbn) VALUES ('9780134685991');
INSERT INTO BookCopy (isbn) VALUES ('9781449370770');
INSERT INTO BookCopy (isbn) VALUES ('9781449316389');
INSERT INTO BookCopy (isbn) VALUES ('9781449325862');
INSERT INTO BookCopy (isbn) VALUES ('9780133761313');

INSERT INTO BookCopy (isbn) VALUES ('9780201633610');
INSERT INTO BookCopy (isbn) VALUES ('9781935504214');
INSERT INTO BookCopy (isbn) VALUES ('9781449374020');
INSERT INTO BookCopy (isbn) VALUES ('9780596527754');
INSERT INTO BookCopy (isbn) VALUES ('9781449374020');
INSERT INTO BookCopy (isbn) VALUES ('9780596527754');
INSERT INTO BookCopy (isbn) VALUES ('9780134685991');
INSERT INTO BookCopy (isbn) VALUES ('9781449370770');
INSERT INTO BookCopy (isbn) VALUES ('9781449325862');
INSERT INTO BookCopy (isbn) VALUES ('9780133761313');
INSERT INTO BookCopy (isbn) VALUES ('9780133761313');
INSERT INTO BookCopy (isbn) VALUES ('9780133761313');


INSERT INTO Author (name) VALUES ('Erich Gamma');
INSERT INTO Author (name) VALUES ('Richard Helm');
INSERT INTO Author (name) VALUES ('Ralph Johnson');
INSERT INTO Author (name) VALUES ('John Vlissides');
INSERT INTO Author (name) VALUES ('Eric Freeman');
INSERT INTO Author (name) VALUES ('Bert Bates');
INSERT INTO Author (name) VALUES ('Kathy Sierra');
INSERT INTO Author (name) VALUES ('Elisabeth Robson');
INSERT INTO Author (name) VALUES ('Kathy Sierra');
INSERT INTO Author (name) VALUES ('Bert Bates');
INSERT INTO Author (name) VALUES ('William Kent');
INSERT INTO Author (name) VALUES ('Paul DuBois');
INSERT INTO Author (name) VALUES ('Maurice Naftalin');
INSERT INTO Author (name) VALUES ('Philip Wadler');
INSERT INTO Author (name) VALUES ('Joshua Bloch');
INSERT INTO Author (name) VALUES ('Richard Warburton');
INSERT INTO Author (name) VALUES ('Jon Loeliger');
INSERT INTO Author (name) VALUES ('Matthew McCullough');
INSERT INTO Author (name) VALUES ('Richard E. Silverman');
INSERT INTO Author (name) VALUES ('Y. Daniel Liang');


INSERT INTO BookAuthor (authorId, isbn) VALUES (1, '9780201633610');
INSERT INTO BookAuthor (authorId, isbn) VALUES (2, '9780201633610');
INSERT INTO BookAuthor (authorId, isbn) VALUES (3, '9780201633610');
INSERT INTO BookAuthor (authorId, isbn) VALUES (4, '9780201633610');
INSERT INTO BookAuthor (authorId, isbn) VALUES (5, '9780596007126');
INSERT INTO BookAuthor (authorId, isbn) VALUES (6, '9780596007126');
INSERT INTO BookAuthor (authorId, isbn) VALUES (7, '9780596007126');
INSERT INTO BookAuthor (authorId, isbn) VALUES (8, '9780596007126');
INSERT INTO BookAuthor (authorId, isbn) VALUES (7, '9780596009205');
INSERT INTO BookAuthor (authorId, isbn) VALUES (6, '9780596009205');
INSERT INTO BookAuthor (authorId, isbn) VALUES (11, '9781935504214');
INSERT INTO BookAuthor (authorId, isbn) VALUES (12, '9781449374020');
INSERT INTO BookAuthor (authorId, isbn) VALUES (13, '9780596527754');
INSERT INTO BookAuthor (authorId, isbn) VALUES (14, '9780596527754');
INSERT INTO BookAuthor (authorId, isbn) VALUES (15, '9780134685991');
INSERT INTO BookAuthor (authorId, isbn) VALUES (16, '9781449370770');
INSERT INTO BookAuthor (authorId, isbn) VALUES (17, '9781449316389');
INSERT INTO BookAuthor (authorId, isbn) VALUES (18, '9781449316389');
INSERT INTO BookAuthor (authorId, isbn) VALUES (19, '9781449325862');
INSERT INTO BookAuthor (authorId, isbn) VALUES (20, '9780133761313');

INSERT INTO Reader (name, address) VALUES ('Mammut Miklós','1024 Budapest, Fény utca 5-7.');


